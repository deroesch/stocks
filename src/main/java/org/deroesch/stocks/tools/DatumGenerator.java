/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.tools;

import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.Datum;
import org.deroesch.stocks.models.SymbolCalendar;
import org.deroesch.stocks.models.exceptions.BadDatumException;

import java.security.SecureRandom;
import java.time.LocalDateTime;

@Slf4j
public class DatumGenerator {

    final SecureRandom randomizer = new SecureRandom();

    /**
     * Returns a random numeric sign (including zero, which means 'no change').
     *
     * @return +1, 0 or -1, randomly
     */
    public int getDirection() {
        return randomizer.nextInt(3) - 1;
    }

    /**
     * Generate a new datum with a small random price fluctuation away from the base datum.
     *
     * @param previous The base datum
     * @return The new datum
     */
    public Datum generateNext(final Datum previous) {
        double high;
        double low;
        final double open = previous.getValue(Datum.CLOSE); // Open is always the previous close
        final int direction = getDirection();    // Rising, falling, or steady, at random

        // New close is the opening price plus a small random perturbation.
        final double close = open + direction * randomizer.nextFloat();

        // Make sure H > O/C > L
        if (open > close) {
            high = close + randomizer.nextFloat();
            low = open - randomizer.nextFloat();
        } else {
            high = open + randomizer.nextFloat();
            low = close - randomizer.nextFloat();
        }

        // Init and return a new datum with the generated prices.
        Datum newDatum = new Datum();
        newDatum.setSymbol(previous.getSymbol());
        newDatum.setPeriod(previous.getPeriod());
        newDatum.setOpen(open);
        newDatum.setClose(close);
        newDatum.setHigh(high);
        newDatum.setLow(low);
        newDatum.setAdjustedClose(close);

        return newDatum;
    }

    /**
     * Fill a calendar with 'howMany' near-random datum elements, given an initial starting period and closing price.
     * The 'next' element of the sequence is a small random delta from the last.
     *
     * @param calendar      Calendar to populate
     * @param initialPeriod First period in the calendar
     * @param initialClose  Closing price of first period
     * @param howMany       Number of Datum objects to store in the calendar
     * @throws BadDatumException When you mix symbols
     */
    public void fillCalendar(final SymbolCalendar calendar,
                             final LocalDateTime initialPeriod,
                             final double initialClose,
                             final int howMany)
            throws BadDatumException {

        // Vars copied in from parameters
        int countRemaining = howMany;

        LocalDateTime currentPeriod = initialPeriod;

        // Starting datum.  We never use this in the live data set.
        // That's why only two fields are set.  It's a seed.
        Datum datum = new Datum();
        datum.setSymbol(calendar.getSymbol());
        datum.setPeriod(currentPeriod);
        datum.setClose(initialClose);

        // Loop until we generate enough data.  Like in the real market,
        // don't use weekend dates.
        while (countRemaining > 0) {

            switch (currentPeriod.getDayOfWeek()) {
                case SUNDAY:
                case SATURDAY:
                    break;

                default:
                    datum = generateNext(datum);
                    datum.setPeriod(currentPeriod);
                    calendar.put(datum);
                    countRemaining--;
            }

            currentPeriod = currentPeriod.plusDays(1L);
        }
    }
}
