/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.tools;

import org.deroesch.stocks.models.Datum;
import org.deroesch.stocks.models.Price;
import org.deroesch.stocks.models.SymbolCalendar;

import java.util.LinkedList;
import java.util.List;

/**
 * Calculator for Simple Moving Average of CLOSING prices.  This one uses stores Datums in a List.
 * A better way might be just to store them in an array, b/c remove() can be a problem...
 */
public class ListBufferSmaCalculator implements Calculator {

    final SymbolCalendar calendar;
    final int[] periodLength;

    /**
     * Calculates simple moving averages for this calendar
     *
     * @param calendar      The calendar
     * @param periodLengths The period lengths to use in calculation, e.g. [5, 10, 20, 50]
     */
    ListBufferSmaCalculator(final SymbolCalendar calendar, final int[] periodLengths) {
        this.calendar = calendar;
        this.periodLength = periodLengths;
    }

    /**
     * Calls apply() for each of the period lengths supplied in the constructor
     */
    @Override
    public void apply() {
        for (final Integer i : periodLength)
            apply(i);
    }

    /**
     * Calculates and saves the SMAs of all Datums in this calendar, for the given period interval
     *
     * @param periodLength The period interval
     */
    public void apply(final int periodLength) {
        double sum = 0.0;
        List<Datum> buffer = new LinkedList<>();
        final String keyName =
                String.format("%s %s(%d)",
                        calendar.getPeriodSize().name(),
                        MovAvg.SMA.name(), periodLength);

        final int size = calendar.size();

        for (int i = 0; i < size; i++) {
            final Datum datum = calendar.get(i);

            buffer.add(datum);
            sum += datum.getValue(Datum.CLOSE);

            if (buffer.size() == periodLength) {
                datum.getStatistics().put(keyName, new Price(keyName, sum / periodLength));

                Datum old = buffer.remove(0);
                sum -= old.getValue(Datum.CLOSE);
            }
        }
    }
}
