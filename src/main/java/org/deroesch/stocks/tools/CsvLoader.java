/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.tools;

import org.deroesch.stocks.models.Datum;
import org.deroesch.stocks.models.SymbolCalendar;
import org.deroesch.stocks.models.exceptions.BadDatumException;
import org.springframework.core.io.Resource;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.List;

/**
 * Creates a symbol calendar from data in a CSV file.
 */
public class CsvLoader {
    final String symbol;
    final Path dataFile;

    /**
     * Path constructor
     *
     * @param symbol   The stock symbol
     * @param dataFile The file of stock data
     */
    public CsvLoader(final String symbol, final Path dataFile) {
        this.symbol = symbol;
        this.dataFile = dataFile;
    }

    /**
     * Resource constructor
     *
     * @param symbol   The stock symbol
     * @param resource The resource containing stock data
     * @throws IOException If I/O fails
     */
    public CsvLoader(final String symbol, final Resource resource) throws IOException {
        this(symbol, resource.getFile().toPath());
    }

    /**
     * Loads the data in the datafile and returns a new SymbolCalendar containing it.
     *
     * @return The new calendar
     * @throws IOException       If I/O fails
     * @throws BadDatumException If the file contains bad data
     */
    public SymbolCalendar load() throws IOException, BadDatumException {
        final List<String> lines = Files.readAllLines(dataFile);
        final SymbolCalendar calendar = new SymbolCalendar(symbol);

        for (String line : lines) {
            if (line.startsWith("Date")) continue;
            calendar.put(Datum.fromCsvLine(symbol, line));
        }

        return calendar;
    }
}
