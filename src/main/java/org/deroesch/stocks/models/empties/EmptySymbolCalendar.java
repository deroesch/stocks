/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models.empties;

import org.deroesch.stocks.models.Datum;
import org.deroesch.stocks.models.PeriodSize;
import org.deroesch.stocks.models.SymbolCalendar;

/**
 * Immutable empty SymbolCalendar singleton
 */
public class EmptySymbolCalendar extends SymbolCalendar {

    // The singleton
    private static final EmptySymbolCalendar SINGLETON = new EmptySymbolCalendar();

    /**
     * Singleton constructor
     */
    private EmptySymbolCalendar() {
        super(Datum.UNKNOWN_SYMBOL, PeriodSize.NONE);
    }

    /**
     * Does nothing.  Empty operation
     *
     * @param datum Ignored
     * @throws UnsupportedOperationException Always throws.  You can't put to an empty calendar.
     */
    @Override
    protected void leafPut(final Datum datum) {
        throw new UnsupportedOperationException("Can't put() to an EmptySymbolCalendar");
    }

    /**
     * Singleton getter
     *
     * @return The singleton
     */
    public static EmptySymbolCalendar get() {
        return SINGLETON;
    }


}
