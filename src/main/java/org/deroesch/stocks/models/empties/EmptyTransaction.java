/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models.empties;

import org.deroesch.stocks.models.Datum;
import org.deroesch.stocks.models.Price;
import org.deroesch.stocks.models.Transaction;

import java.time.LocalDateTime;

public class EmptyTransaction extends Transaction {

    private static final Transaction SINGLETON = new EmptyTransaction();

    /**
     * Singleton constructor
     */
    private EmptyTransaction() {
        super(Datum.UNKNOWN_SYMBOL,
                LocalDateTime.now(),
                0,
                new Price(Datum.CLOSE, 0.0));
    }

    /**
     * Singleton getter
     *
     * @return The singleton
     */
    public static Transaction get() {
        return SINGLETON;
    }
}
