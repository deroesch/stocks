/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.empties.EmptyDatum;
import org.deroesch.stocks.models.empties.EmptyPrice;

import java.time.DayOfWeek;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;

@Slf4j
@Getter
@Setter
public class Datum {
    public static final String UNKNOWN_SYMBOL = "????";

    public static final String HIGH = "HIGH";
    public static final String LOW = "LOW";
    public static final String OPEN = "OPEN";
    public static final String CLOSE = "CLOSE";
    public static final String ADJUSTED_CLOSE = "ADJUSTED_CLOSE";

    protected String symbol;
    protected LocalDateTime period;

    private Map<String, Price> priceMap = new HashMap<>();

    private long volume;

    private Map<String, Price> statistics = new HashMap<>();

    /**
     * Empty constructor, for testing (mostly).
     */
    public Datum() {
        this(Datum.UNKNOWN_SYMBOL, LocalDateTime.now(), 0.0, 0.0, 0.0, 0.0, 0.0, 0);
    }

    /**
     * Field constructor
     *
     * @param symbol        The datum's symbol
     * @param period        The datum's period
     * @param high          The datum's high price
     * @param low           The datum's low price
     * @param open          The datum's opening price
     * @param close         The datum's closing price
     * @param adjustedClose The datum's adjusted closing price
     * @param volume        The datum's volume
     */
    public Datum(final String symbol,
                 final LocalDateTime period,
                 final double high,
                 final double low,
                 final double open,
                 final double close,
                 final double adjustedClose,
                 final long volume) {
        this.symbol = symbol;
        this.period = period;

        priceMap.put(HIGH, new Price(HIGH, high));
        priceMap.put(LOW, new Price(LOW, low));
        priceMap.put(OPEN, new Price(OPEN, open));
        priceMap.put(CLOSE, new Price(CLOSE, close));
        priceMap.put(ADJUSTED_CLOSE, new Price(ADJUSTED_CLOSE, adjustedClose));

        this.volume = volume;
    }

    /**
     * CSV Line Constructor
     *
     * @param symbol The stock symbol
     * @param line   String of the form "Date,Open,High,Low,Close,Adj Close,Volume", date is of the form "yyyy-mm-dd"
     * @return A datum containing the given fields
     */
    public static Datum fromCsvLine(final String symbol, final String line) {
        final String[] allFields = line.split(",");
        final String[] dateFields = allFields[0].split("-");

        int year = Integer.parseInt(dateFields[0]);
        int month = Integer.parseInt(dateFields[1]);
        int day = Integer.parseInt(dateFields[2]);

        Datum datum = new Datum();
        datum.setSymbol(symbol);
        datum.setPeriod(LocalDateTime.of(year, month, day, 4, 0));

        int i = 1;
        datum.setOpen(Float.parseFloat(allFields[i++]));
        datum.setHigh(Float.parseFloat(allFields[i++]));
        datum.setLow(Float.parseFloat(allFields[i++]));
        datum.setClose(Float.parseFloat(allFields[i++]));
        datum.setAdjustedClose(Float.parseFloat(allFields[i++]));
        datum.setVolume(Integer.parseInt(allFields[i++]));

        return datum;
    }

    /**
     * Get this datum's price for the given role
     *
     * @param role The role
     * @return The price for the role, or the EmptyPrice is not found.
     */
    public Price getPrice(final String role) {
        switch (role) {
            case HIGH:
            case LOW:
            case OPEN:
            case CLOSE:
            case ADJUSTED_CLOSE:
                return priceMap.getOrDefault(role, EmptyPrice.get());
            default:
                return getStatistics().getOrDefault(role, EmptyPrice.get());
        }
    }

    /**
     * Find the value of the price for the given role.
     *
     * @param role The role played by this price (HIGH, LOW, OPEN, etc.).
     * @return The value of the price
     */
    public double getValue(final String role) {
        return getPrice(role).getValue();
    }

    /**
     * Is this an empty value?
     *
     * @return True if this is an empty Datum, false otherwise
     */
    public boolean isEmpty() {
        return this.equals(EmptyDatum.get());
    }

    /**
     * Get a human-readable representation of this datum's period.
     *
     * @return A human-readable period string
     */
    public String getPrintedPeriod() {
        return period.truncatedTo(ChronoUnit.MINUTES).toString().replace('T', '_');
    }

    /**
     * Get a human-readable representation of this datum's period, truncated to the given unit
     *
     * @return A human-readable period string truncated to the given unit
     */
    public String getPrintedPeriod(ChronoUnit truncatedTo) {
        return period.truncatedTo(truncatedTo).toString().replace('T', '_');
    }

    /**
     * Get the price change from the previous period
     *
     * @return the price change from the previous period
     */
    public double getPriceChange() {
        return getValue(CLOSE) - getValue(OPEN);
    }

    /**
     * Get the percentage price change from the previous period
     *
     * @return the percentage price change from the previous period
     */
    public double getPriceChangePercent() {
        return (getValue(CLOSE) - getValue(OPEN)) / getValue(OPEN);
    }

    /**
     * Is this datum is the start of a week?
     *
     * @return True if this datum is the start of a week, false otherwise
     */
    public boolean isStartOfWeek() {
        return period.getDayOfWeek().equals(DayOfWeek.MONDAY);
    }

    /**
     * Is this datum is the end of a week?
     *
     * @return True if this datum is the end of a week, false otherwise
     */
    public boolean isEndOfWeek() {
        return period.getDayOfWeek().equals(DayOfWeek.FRIDAY);
    }

    /**
     * Does this date fall on a weekend?
     *
     * @return True if this date falls on a weekend, false otherwise
     */
    public boolean isWeekend() {
        final DayOfWeek day = period.getDayOfWeek();
        return DayOfWeek.SATURDAY == day || DayOfWeek.SUNDAY == day;
    }

    /**
     * Does this date fall on a week day?
     *
     * @return True if this date falls on a week day, false otherwise
     */
    public boolean isWeekday() {
        return !isWeekend();
    }

    /**
     * Is this datum is the start of a month?
     *
     * @return True if this datum is the start of a month, false otherwise
     */
    public boolean isStartOfMonth() {
        return period.getDayOfMonth() == 1;
    }

    /**
     * Is this datum is the end of a month?
     *
     * @return True if this datum is the end of a month, false otherwise
     */
    public boolean isEndOfMonth() {
        return period.plusDays(1).getDayOfMonth() == 1;
    }

    /**
     * Sets the HIGH value for this datum.
     *
     * @param value the value to set the value to set
     */
    public void setHigh(final double value) {
        priceMap.put(HIGH, new Price(HIGH, value));
    }

    /**
     * Sets the LOW value for this datum.
     *
     * @param value the value to set
     */
    public void setLow(final double value) {
        priceMap.put(LOW, new Price(LOW, value));
    }

    /**
     * Sets the OPEN value for this datum.
     *
     * @param value the value to set
     */
    public void setOpen(final double value) {
        priceMap.put(OPEN, new Price(OPEN, value));
    }

    /**
     * Sets the CLOSE value for this datum.
     *
     * @param value the value to set
     */
    public void setClose(final double value) {
        priceMap.put(CLOSE, new Price(CLOSE, value));
    }

    /**
     * Sets the ADJUSTED_CLOSE value for this datum.
     *
     * @param value the value to set
     */
    public void setAdjustedClose(final double value) {
        priceMap.put(ADJUSTED_CLOSE, new Price(ADJUSTED_CLOSE, value));
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Datum.class.getSimpleName() + "[", "]")
                .add("Symbol: " + getSymbol())
                .add("Day: " + getPeriod().getDayOfWeek().toString())
                .add("Period: " + getPrintedPeriod())
                .add("Delta:" + String.format(Price.FORMAT, getPriceChange()))
                .add("Open:" + getPrice(OPEN))
                .add("Close:" + getPrice(CLOSE))
                .add("High:" + getPrice(HIGH))
                .add("Low:" + getPrice(LOW))
                .add("AdjClose:" + getPrice(ADJUSTED_CLOSE))
                .add("Volume:" + volume)
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Datum datum = (Datum) o;
        return Price.compare(datum.getPrice(HIGH), getPrice(HIGH)) == 0
                && Price.compare(datum.getPrice(LOW), getPrice(LOW)) == 0
                && Price.compare(datum.getPrice(OPEN), getPrice(OPEN)) == 0
                && Price.compare(datum.getPrice(CLOSE), getPrice(CLOSE)) == 0
                && Price.compare(datum.getPrice(ADJUSTED_CLOSE), getPrice(ADJUSTED_CLOSE)) == 0
                && volume == datum.volume
                && symbol.equals(datum.symbol)
                && period.equals(datum.period);
    }

    @Override
    public int hashCode() {
        return Objects.hash(symbol, period, getValue(HIGH), getValue(LOW), getValue(OPEN), getValue(CLOSE), getValue(ADJUSTED_CLOSE), volume);
    }


}

