/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.empties.EmptyPosition;
import org.deroesch.stocks.models.exceptions.ClosedPositionException;
import org.deroesch.stocks.models.exceptions.DateOrderException;
import org.deroesch.stocks.models.exceptions.NonEmptyAssignmentException;
import org.deroesch.stocks.models.exceptions.SymbolMismatchException;
import org.deroesch.utils.LogUtils;
import org.deroesch.utils.StringUtils;

import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * A stock position
 */
@Slf4j
@Getter
public class Position {
    private final String id;
    private final Account account;
    private final String symbol;
    private final LocalDateTime openDate;
    private LocalDateTime closeDate;
    private long shares;
    private double amount;
    private double profit;
    private State state;
    private final List<Transaction> transactions = new LinkedList<>();

    // Position lifecycle states
    public enum State {NEW, OPEN, CLOSED}

    /**
     * Field constructor
     *
     * @param account  The account owning this position
     * @param symbol   The position's symbol
     * @param openDate The date the position was opened
     */
    public Position(final Account account,
                    final String symbol,
                    final LocalDateTime openDate) {
        this(StringUtils.newUUID(), account, symbol, openDate);
    }

    /**
     * Hidden constructor with ID, for testing.
     *
     * @param id       Fixed ID for testing
     * @param account  The account owning this position
     * @param symbol   The position's symbol
     * @param openDate The date the position was opened
     */
    Position(final String id,
             final Account account,
             final String symbol,
             final LocalDateTime openDate) {
        this.id = id;
        this.account = account;
        this.symbol = symbol;
        this.openDate = openDate;
        this.state = State.NEW;
    }

    /**
     * Is this an open position?
     *
     * @return true if this an open position, false otherwise
     */
    public boolean isOpen() {
        return account.getOpenPositions().containsKey(this.symbol);
    }

    /**
     * Is this a closed position?
     *
     * @return if this a closed position, false otherwise
     */
    public boolean isClosed() {
        return account.getClosedPositions().containsKey(this.symbol);
    }

    /**
     * Set position state according to presence or absence on open / closed lists
     */
    private void updateStateMarker() {
        state = isOpen() ? State.OPEN : state;
        state = isClosed() ? State.CLOSED : state;
    }

    /**
     * Add this transaction to the position.
     *
     * @param tx The transaction to add
     * @throws SymbolMismatchException     if position and transaction have non-matching symbols
     * @throws NonEmptyAssignmentException if transaction was already assigned to another position
     */
    public void addTransaction(final Transaction tx) throws SymbolMismatchException, NonEmptyAssignmentException {

        // Symbols don't match?
        if (!tx.getSymbol().equals(this.getSymbol())) {
            final String template = "Transaction with symbol '%s' can't be assigned to a Position with symbol '%s'.";
            final String message = String.format(template, tx.getSymbol(), this.getSymbol());
            throw new SymbolMismatchException(message);
        }

        // Transaction already part of another position?
        if (!tx.getPosition().isEmpty()) {
            final String template = "Transaction '%s' was already assigned to Position '%s' and can't be reassigned.";
            final String message = String.format(template, tx.getId(), this.getId());
            throw new NonEmptyAssignmentException(message);
        }

        // Assign the transaction to this position
        tx.setPosition(this);
        this.transactions.add(tx);
        updateStateMarker();

        // Update $ and share balances
        this.amount += tx.getAmount();
        this.shares += tx.getShares();

        // Log the transaction
        final String direction = tx.getAmount() > 0 ? "BOUGHT" : "SOLD  ";
        if (direction.equals("SOLD  "))
            this.profit = -1 * this.amount;

        final String template = "Account: '%s', Position: '%s', %s %s shares @ $%s";
        final String message = String.format(
                template,
                this.account.getId(),
                this.getSymbol(),
                direction,
                Math.abs(tx.getShares()),
                tx.getPrice().getValue());
        LogUtils.always(log, message);
    }

    /**
     * Close this position, i.e., sell all shares on the closeDate at the sellingPrice.
     *
     * @param closeDate    The date of sale
     * @param sellingPrice The selling price per share
     * @throws DateOrderException      When given closing date is before the opening date
     * @throws ClosedPositionException When closing a position that's already closed
     */
    public void close(final LocalDateTime closeDate, final Price sellingPrice) throws DateOrderException, ClosedPositionException {

        // Already closed?
        if (this.closeDate != null)
            throw new ClosedPositionException("This position was already closed, on " + this.closeDate);

        // Bad close date?
        if (closeDate.isBefore(this.openDate))
            throw new DateOrderException(
                    "You can't close a position before you open it.  Check your dates!");

        // Create and apply a compensating transaction to sell all shares.
        final Transaction sellTransaction =
                new Transaction(
                        this.symbol,
                        closeDate,
                        (-1 * this.shares),
                        sellingPrice);

        try {
            account.addTransaction(sellTransaction);
        } catch (NonEmptyAssignmentException | SymbolMismatchException e) {
            // NonEmptyAssignmentException won't happen -- no assignment done here
            // SymbolMismatchException won't happen -- position symbol used for construction
        }

        // Final bookkeeping: set close date, move position to closed list, reset position state.
        this.closeDate = closeDate;
        getAccount().getOpenPositions().remove(this.symbol);
        getAccount().getClosedPositions().put(this.symbol, this);
        this.updateStateMarker();
    }

    /**
     * Is this the EmptyPosition?
     *
     * @return true if this position is empty, false otherwise.
     */
    public boolean isEmpty() {
        return this.equals(EmptyPosition.get());
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Position.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("account=" + account)
                .add("symbol='" + symbol + "'")
                .add("openDate=" + openDate)
                .add("closeDate=" + closeDate)
                .add("shares=" + shares)
                .add("amount=" + amount)
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        return this == o;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, account, symbol, openDate, closeDate, shares, amount, transactions);
    }
}
