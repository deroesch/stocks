/*
 * Copyright (c) 2021-2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.empties.EmptyDatum;
import org.deroesch.stocks.models.empties.EmptySymbolCalendar;
import org.deroesch.stocks.models.exceptions.BadDatumException;
import org.deroesch.utils.LogUtils;

import java.time.LocalDateTime;
import java.util.*;

@Slf4j
@Getter
public class SymbolCalendar {

    private final String symbol;
    private LocalDateTime firstPeriod = LocalDateTime.MAX;
    private LocalDateTime finalPeriod = LocalDateTime.MIN;
    private int lastIndex = 0;

    /**
     * Constructor for the default period size of DAY.
     *
     * @param symbol The symbols associated with this calendar.  Will only accept datams for this symbol.
     */
    public SymbolCalendar(final String symbol) {
        this(symbol, PeriodSize.DAY);
    }

    /**
     * Constructor for any period size
     *
     * @param symbol     The symbols associated with this calendar.  Will only accept datams for this symbol
     * @param periodSize One of PeriodSize: DAY, WEEK, MONTH
     */
    public SymbolCalendar(final String symbol, final PeriodSize periodSize) {
        this.symbol = symbol;
        this.periodSize = periodSize;
    }

    /**
     * Is this calendar empty?
     *
     * @return true if the calendar is empty, false otherwise.
     */
    public boolean isEmpty() {
        return this.equals(EmptySymbolCalendar.get());
    }

    /**
     * What's the first date in this calendar?
     *
     * @return the first date in this calendar
     */
    public LocalDateTime getFirstPeriod() {
        return firstPeriod;
    }

    /**
     * What's the last date in this calendar?
     *
     * @return the last date in this calendar
     */
    public LocalDateTime getFinalPeriod() {
        return finalPeriod;
    }

    /**
     * Get the datum at date
     *
     * @param date Target date
     * @return Datum at that date, or the EmptyDate if no target date exists
     */
    public Datum get(final LocalDateTime date) {
        final Datum d = historyByDate.get(date);
        return d != null ? d : EmptyDatum.get();
    }

    /**
     * Get the datum at index
     *
     * @param index The index
     * @return The datum at that index, or the EmptyDate if no target date exists
     */
    public Datum get(final int index) {
        Datum datum = EmptyDatum.get();
        if (index < historyByIndex.size()) {
            datum = historyByIndex.get(index);
        }
        return datum;
    }


    /**
     * Put a datum into this calendar.  This is not random-access.  Index of
     * this datum will be last-index + 1.  Make sure you insert them in date order
     * or the math won't work.
     *
     * @param datum The datum to insert
     * @throws BadDatumException On insertion of bad data.
     */
    public void put(final Datum datum) throws BadDatumException {
        leafPut(datum);

        if (datum.isEndOfWeek())
            getWeeklyCalendar().leafPut(datum);

        if (datum.isEndOfMonth())
            getMonthlyCalendar().leafPut(datum);
    }


    /**
     * Inserts a datum by its date.
     *
     * @param datum The datum to insert
     * @throws BadDatumException On insertion of bad data.
     */
    protected void leafPut(final Datum datum) throws BadDatumException {
        if (datum.getSymbol().equals(symbol)) {
            if (datum.isWeekend()) {
                LogUtils.always(log, "");
                LogUtils.always(log, String.format("Inserting a weekend date: %s", datum));
                LogUtils.always(log, "Are you sure you meant to do that?");
                LogUtils.always(log, "");
            }

            initSubCalendars();

            historyByDate.put(datum.getPeriod(), datum);
            historyByIndex.add(lastIndex++, datum);  // It's your problem to insert them in the proper order...

            // Reset max?
            if (datum.getPeriod().isBefore(firstPeriod))
                firstPeriod = datum.getPeriod();

            // Reset min?
            if (datum.getPeriod().isAfter(finalPeriod))
                finalPeriod = datum.getPeriod();

        } else
            throw new BadDatumException(datum);
    }

    /**
     * Get the weekly calendar, initialize if empty.
     *
     * @return the weekly calendar
     */
    public SymbolCalendar getWeeklyCalendar() {
        initSubCalendars();
        return weeklyCalendar;
    }

    /**
     * Get the monthly calendar, initialize if empty.
     *
     * @return the monthly calendar
     */
    public SymbolCalendar getMonthlyCalendar() {
        initSubCalendars();
        return monthlyCalendar;
    }

    /**
     * Initialize the weekly and monthly calendars.
     */
    private void initSubCalendars() {
        if (null == weeklyCalendar)
            weeklyCalendar = PeriodSize.DAY == periodSize
                    ? new SymbolCalendar(getSymbol(), PeriodSize.WEEK)
                    : EmptySymbolCalendar.get();

        if (null == monthlyCalendar)
            monthlyCalendar = (PeriodSize.DAY == periodSize)
                    ? new SymbolCalendar(getSymbol(), PeriodSize.MONTH)
                    : EmptySymbolCalendar.get();
    }

    /**
     * How many Datums are in this calendar
     *
     * @return The size of the calendar
     */
    public int size() {
        return historyByDate.size();
    }

    /**
     * Get the contents of this calendar as an iterator
     *
     * @return The iterator
     */
    public Iterator<String> getHistoryAsStrings() {
        final String[] lines = new String[historyByDate.size()];
        int i = 0;
        for (final Map.Entry<LocalDateTime, Datum> entry : historyByDate.entrySet())
            lines[i++] = historyByDate.get(entry.getKey()).toString();
        return Arrays.stream(lines).iterator();
    }

    /**
     * Print the contents of this calendar to the console
     */
    public void printHistory() {
        LogUtils.verbose(log);
        final Iterator<String> lines = getHistoryAsStrings();
        while (lines.hasNext())
            LogUtils.verbose(log, lines.next());
    }

    /**
     * Print the chart for this calender to the console
     */
    public void printChart() {
        final double startingPrice = get(1).getValue(Datum.OPEN);

        double high = startingPrice;
        double low = startingPrice;
        double diff = 0.0d;

        int upticks = 0;
        int downticks = 0;

        StringBuilder b = new StringBuilder();
        for (Map.Entry<LocalDateTime, Datum> entry : historyByDate.entrySet()) {
            final Datum datum = entry.getValue();
            final double delta = datum.getPriceChange();

            char direction;
            direction = (delta < 0) ? '\\' : '_';
            direction = (delta > 0) ? '/' : direction;
            b.append(direction);

            if (delta > 0) upticks++;
            if (delta < 0) downticks++;

            high = Math.max(high, datum.getValue(Datum.HIGH));
            low = Math.min(low, datum.getValue(Datum.LOW));
            diff = high - low;
        }

        final String fluctuations = b.toString();

        LogUtils.verbose(log, fluctuations);
        LogUtils.verbose(log, String.format("High: %s, Low: %s, Diff: %s", high, low, diff));

        final int xAxisSize = historyByDate.size();
        final int yAxisSize = upticks + downticks + 2; // Maximum possible vertical space needed

        // Big array of stock chart characters, to be printed.  A 1970's version of a graph.
        final String[][] stockChart = new String[xAxisSize][yAxisSize];

        int offset = downticks + 1;
        int topIndex = offset;
        int bottomIndex = offset;

        // Calculate top and bottom margins
        for (int i = 0; i < xAxisSize; i++) {
            final String c = String.valueOf(fluctuations.charAt(i));

            stockChart[i][offset] = c;

            switch (c) {
                case "/":
                    offset++;
                    bottomIndex = Math.max(bottomIndex, offset);
                    break;

                case "\\":
                    offset--;
                    topIndex = Math.min(topIndex, offset);
                    break;

                default:
                    // Do nothing
            }
        }

        printBuffers(stockChart, xAxisSize, yAxisSize, topIndex, bottomIndex);
    }

    /**
     * @param chart
     * @param xAxisSize
     * @param yAxisSize
     * @param minOffset
     * @param maxOffset
     */
    private void printBuffers(
            final String[][] chart,
            final int xAxisSize,
            final int yAxisSize,
            final int minOffset,
            final int maxOffset) {
        StringBuffer[] rows = new StringBuffer[yAxisSize];
        for (int i = 0; i < yAxisSize; i++) {
            rows[i] = new StringBuffer();
            for (int j = 0; j < xAxisSize; j++)
                rows[i].append(chart[j][i] == null ? " " : chart[j][i]);
        }

        final char[] chars = new char[xAxisSize];
        Arrays.fill(chars, '_');
        final String border = new String(chars);

        // Top border
        LogUtils.always(log, String.format("_%s_", border));

        // Go from the top down, since pages print top-to-bottom.  Start
        // with the highest buffer that isn't empty, then print until we
        // hit the next empty buffer.  (Would have been an awful lot easier
        // doing this with an array of 'empty row' flags.  Don't print
        // empty rows.  I did it using offsets just to give myself grief.)
        for (int i = maxOffset + 1; i > minOffset - 1; i--)
            LogUtils.always(log, String.format("|%s|", rows[i].toString()));

        // Bottom border
        LogUtils.always(log, String.format("|%s|", border));
    }

    @Setter(AccessLevel.NONE)
    private final PeriodSize periodSize;

    /**
     * History ordered by calendar date.
     */
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private final Map<LocalDateTime, Datum> historyByDate = new TreeMap<>();

    /**
     * History by order of insertion (when using put()).
     */
    @Getter(AccessLevel.NONE)
    @Setter(AccessLevel.NONE)
    private final List<Datum> historyByIndex = new LinkedList<>();

    /**
     * End of week calendar (ever Friday).
     */
    @Setter(AccessLevel.NONE)
    private SymbolCalendar weeklyCalendar;

    /**
     * End of month dates (Month-1st minus one day).
     */
    @Setter(AccessLevel.NONE)
    private SymbolCalendar monthlyCalendar;
}
