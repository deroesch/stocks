/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.exceptions.NonEmptyAssignmentException;
import org.deroesch.stocks.models.exceptions.SymbolMismatchException;
import org.deroesch.utils.StringUtils;

import java.util.Map;
import java.util.Objects;
import java.util.StringJoiner;
import java.util.TreeMap;

/**
 * A user financial account for storing stock positions
 */
@Slf4j
@Getter
public class Account {
    enum State {IDLE, BUYING, HOLDING, SELLING, SETTLING}

    private final String id;
    private final State state;
    private double balance;

    private final Map<String, Position> openPositions = new TreeMap<>();
    private final Map<String, Position> closedPositions = new TreeMap<>();

    /**
     * Field constructor
     *
     * @param balance Opening cash balance.
     */
    public Account(final double balance) {
        this(StringUtils.newUUID(), balance);
    }

    /**
     * Hidden constructor with ID, for testing.
     *
     * @param id      Fixed ID for testing
     * @param balance Opening cash balance
     */
    Account(final String id, final double balance) {
        this.id = id;
        this.balance = balance;
        this.state = State.IDLE;
    }

    /**
     * Records a BUY/SELL transaction onto a known position, or opens a new position if none exists.
     *
     * @param transaction The transaction to add
     */
    public void addTransaction(final Transaction transaction) throws NonEmptyAssignmentException, SymbolMismatchException {
        final String symbol = transaction.getSymbol();

        // Create a new position if missing
        openPositions.computeIfAbsent(symbol, key -> new Position(this, key, transaction.getTimestamp()));

        // Add the transaction to the position
        openPositions.get(symbol).addTransaction(transaction);

        // Adjust the balance
        balance -= transaction.getAmount();
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Account.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("state=" + state)
                .add("balance=" + balance)
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        return this == o;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, state, balance, openPositions, closedPositions);
    }
}
