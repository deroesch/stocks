/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import lombok.Getter;
import org.deroesch.stocks.models.empties.EmptyPrice;

import java.util.Objects;

@Getter
public class Price {

    public static final String FORMAT = "%5.2f";

    private final double value;
    private final String role;

    /**
     * Field constructor
     *
     * @param role  The role this price plays (HIGH, LOW, SMA(5), etc.)
     * @param value The values of this price
     */
    public Price(final String role, final double value) {
        this.role = role;
        this.value = value;
    }

    /**
     * Is this the Empty Price?
     *
     * @return true if this is the empty price, false otherwise.
     */
    public boolean isEmpty() {
        return this.fullyEquals(EmptyPrice.get());
    }

    /**
     * Compares two prices by comparing their values.
     *
     * @param p1 First price to compare
     * @param p2 Second prices to compare
     * @return Directional result, i.e., result &lt; 0, result == 0, result &gt; 0.
     */
    public static int compare(final Price p1, final Price p2) {
        return Double.compare(p1.getValue(), p2.getValue());
    }

    /**
     * Is this price above the comparison price?
     *
     * @param price The comparison price
     * @return true if this prices is greater than the comparison price, false otherwise
     */
    public boolean isAbove(final Price price) {
        return compare(this, price) > 0;
    }

    /**
     * Is this price below the comparison price?
     *
     * @param price The comparison price
     * @return true if this prices is less than the comparison price, false otherwise
     */
    public boolean isBelow(final Price price) {
        return compare(this, price) < 0;
    }

    /**
     * Price changes indicator labels for this price relative to another.
     */
    public enum Compares {HIGHER, STEADY, LOWER}

    /**
     * Compare this price to another price and return the relative Compares direction.
     *
     * @param price The comparison price
     * @return A Compares value
     */
    public Compares direction(final Price price) {
        Compares direction = Compares.STEADY;
        if (this.isAbove(price)) return Compares.HIGHER;
        if (this.isBelow((price))) return Compares.LOWER;
        return direction;
    }

    /**
     * Price comparison involves values only.  Name is ignored.  If values are equal, prices are equal.
     */
    @Override
    public boolean equals(final Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Price price = (Price) o;
        return Double.compare(price.value, value) == 0;
    }

    /**
     * Same as equals() but compares names as well.
     *
     * @param o The comparison price
     * @return True if prices both values and names are equal, false otherwise.
     */
    public boolean fullyEquals(final Price o) {
        return equals(o) && Objects.equals(role, o.role);
    }

    /**
     * Return this price plus or minus a percentage.
     *
     * @param percentage The percentage as a decimal.  Enter 1% as 0.01.
     * @return The price plus the percent change.
     */
    public double plusPercent(final double percentage) {
        return value + (value * percentage);
    }

    @Override
    public int hashCode() {
        return Objects.hash(value, role);
    }

    @Override
    public String toString() {
        return String.format(FORMAT, getValue());
    }
}
