/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;


import lombok.AccessLevel;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.empties.EmptyPosition;
import org.deroesch.stocks.models.empties.EmptyTransaction;
import org.deroesch.utils.StringUtils;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.StringJoiner;

/**
 * A single BUY/SELL transaction
 */
@Slf4j
@Getter
public class Transaction {

    private final String id;
    private final String symbol;
    private final LocalDateTime timestamp;
    private final long shares;
    private final Price price;

    @Setter(AccessLevel.PACKAGE)
    private Position position;  // The owning position.

    /**
     * Field constructor
     *
     * @param symbol    The symbol of the security involved
     * @param timestamp Timestamp of the BUY/SELL event
     * @param shares    Number of shares in the transaction, can be + or - .
     * @param price     The price of the security at the time of the event
     */
    public Transaction(final String symbol,
                       final LocalDateTime timestamp,
                       final long shares,
                       final Price price) {
        this(StringUtils.newUUID(), symbol, timestamp, shares, price);
    }

    /**
     * Hidden constructor with ID, for testing
     *
     * @param id        Fixed ID for testing
     * @param symbol    The symbol of the security involved
     * @param timestamp Timestamp of the BUY/SELL event
     * @param shares    Number of shares in the transaction, can be + or - .
     * @param price     The price of the security at the time of the event
     */
    Transaction(final String id,
                final String symbol,
                final LocalDateTime timestamp,
                final long shares,
                final Price price) {
        this.id = id;
        this.symbol = symbol;
        this.timestamp = timestamp;
        this.shares = shares;
        this.price = price;
        this.position = EmptyPosition.get();
    }


    /**
     * Is this Transaction empty?
     *
     * @return true if this transaction is empty, false otherwise
     */
    public boolean isEmpty() {
        return this.equals(EmptyTransaction.get());
    }

    /**
     * Returns the trade amount, shares X price
     *
     * @return the trade amount
     */
    public double getAmount() {
        return price.getValue() * shares;
    }

    @Override
    public String toString() {
        return new StringJoiner(", ", Transaction.class.getSimpleName() + "[", "]")
                .add("id='" + id + "'")
                .add("symbol='" + symbol + "'")
                .add("timestamp=" + timestamp)
                .add("shares=" + shares)
                .add("price=" + price)
                .add("amount=" + getAmount())
                .toString();
    }

    @Override
    public boolean equals(final Object o) {
        return this == o;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, position, symbol, timestamp, shares, price);
    }
}
