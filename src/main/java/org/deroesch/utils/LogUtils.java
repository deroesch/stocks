/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.utils;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;

/**
 * Simplest possible log utilities
 */
@Slf4j
public class LogUtils {

    private static boolean verbose = false;

    /**
     * Private constructor ot prevent instantiation
     */
    private LogUtils() {
        super();
    }

    /**
     * Always write a blank line to the log
     *
     * @param log The destination log
     */
    public static void always(Logger log) {
        always(log, "");
    }

    /**
     * Always write the message to the log
     *
     * @param log     The destination log
     * @param message The message to write
     */
    public static void always(Logger log, String message) {
        log.info(message);
    }

    /**
     * Write a blank line to the log when 'verbose' flag is 'true'
     *
     * @param log The destination log
     */
    public static void verbose(Logger log) {
        verbose(log, "");
    }

    /**
     * Write the message to the log when 'verbose' flag is 'true'
     *
     * @param log     The destination log
     * @param message The message to write
     */
    public static void verbose(Logger log, String message) {
        if (verbose) log.info(message);
    }

    /**
     * Is the 'verbose' flag true?
     *
     * @return true if 'verbose' flag is true, false otherwise
     */
    public static boolean isVerbose() {
        return verbose;
    }

    /**
     * Set the verbose flag
     *
     * @param verbose The value to set
     */
    public static void setVerbose(boolean verbose) {
        LogUtils.verbose = verbose;
    }
}
