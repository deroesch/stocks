/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.utils;

import java.security.SecureRandom;
import java.util.UUID;

/**
 * Simplest possible String utilities
 */
public class StringUtils {

    private static final String CHARS = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_+=-[]\\\"|}{;':,./?><";
    private static final int NUM_CHARS = CHARS.length();
    private static final SecureRandom RNG = new SecureRandom();

    /**
     * Private constructor ot prevent instantiation
     */
    private StringUtils() {
        super();
    }

    /**
     * Returns a UUID
     *
     * @return a UUID
     */
    public static String newUUID() {
        return UUID.randomUUID().toString();
    }

    /**
     * Return a random string without special characters
     *
     * @param length String length
     * @return the string
     */
    public static String randomString(final int length) {
        return randomString(length, false);
    }

    /**
     * Return a random string, with or without special characters
     *
     * @param length                String length
     * @param withSpecialCharacters use special characters in the string?
     * @return the string
     */
    public static String randomString(final int length, final boolean withSpecialCharacters) {
        final int limit = withSpecialCharacters ? NUM_CHARS : 26 + 26 + 10;
        StringBuilder b = new StringBuilder();
        for (int i = 0; i < length; i++)
            b.append(CHARS.charAt(RNG.nextInt(limit)));
        return b.toString();
    }
}
