
/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.tools;

import org.deroesch.stocks.models.Datum;
import org.deroesch.stocks.models.PeriodSize;
import org.deroesch.stocks.models.SymbolCalendar;
import org.deroesch.stocks.models.empties.EmptyPrice;
import org.deroesch.stocks.models.exceptions.BadDatumException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class SmaCalculatorTest {

    SymbolCalendar calender;

    @BeforeEach
    void setUp() {
        calender = new SymbolCalendar("ATT");
    }

    @Test
    void apply() throws BadDatumException {
        final int howMany = 28;

        final LocalDateTime startDate = LocalDateTime.of(2021, 8, 2, 0, 0);
        new DatumGenerator().fillCalendar(calender, startDate, 100, howMany);

        final int[] calcPeriods = new int[]{5, 10, 20};
        new ListBufferSmaCalculator(calender, calcPeriods).apply();
        new ListBufferSmaCalculator(calender.getWeeklyCalendar(), calcPeriods).apply();
        new ListBufferSmaCalculator(calender.getMonthlyCalendar(), calcPeriods).apply();

        //TODO Flesh this out with custom calendars

        final Datum d = calender.get(howMany - 1);
        assertFalse(d.isEmpty());
        assertTrue(d.getPrice(Datum.HIGH).getValue() > 0.0);

        String keyName = String.format("%s %s", PeriodSize.DAY.name(), MovAvg.SMA.keyName(5));
        assertTrue(d.getValue(keyName) > 0.0);

        keyName = String.format("%s %s", PeriodSize.DAY.name(), MovAvg.SMA.keyName(10));
        assertTrue(d.getValue(keyName) > 0.0);

        keyName = String.format("%s %s", PeriodSize.DAY.name(), MovAvg.SMA.keyName(20));
        assertTrue(d.getValue(keyName) > 0.0);

        keyName = String.format("%s %s", PeriodSize.DAY.name(), MovAvg.SMA.keyName(50));
        assertEquals(EmptyPrice.get(), d.getPrice(keyName));
    }
}