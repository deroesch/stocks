/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.tools;

import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.Datum;
import org.deroesch.stocks.models.SymbolCalendar;
import org.deroesch.stocks.models.exceptions.BadDatumException;
import org.deroesch.utils.LogUtils;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.ZoneId;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class DatumGeneratorTest {

    DatumGenerator generator;

    int positive = 0;
    int negative = 0;
    int neutral = 0;

    @BeforeEach
    void setUp() {
        generator = new DatumGenerator();
    }

    /**
     * getDirection() should return 1, 0, or -1 randomly and evenly distributed.  For large
     * iterations, the appearance frequency for each of these values should be similar.
     */
    @Test
    void getDirection() {
        for (int i = 0; i < 100000; i++)
            switch (generator.getDirection()) {
                case 1:
                    positive++;
                    break;

                case -1:
                    negative++;
                    break;

                default:
                    neutral++;
                    break;
            }

        LogUtils.always(log, "These numbers should be within 1000 points of each other:");
        LogUtils.always(log, String.format("+:%d, 0:%d, -:%d", positive, neutral, negative));
        assertTrue(Math.abs(positive - negative) < 1000);
        assertTrue(Math.abs(negative - neutral) < 1000);
    }

    @Test
    void generateNext() {
        final Datum prev = new Datum("ABC", LocalDateTime.now(), 1, 2, 3, 4, 5, 6);
        final Datum next = generator.generateNext(prev);

        assertNotNull(next);
        assertEquals(prev.getSymbol(), next.getSymbol());
        assertEquals(prev.getPeriod(), next.getPeriod());
        assertTrue(next.getPriceChange() < 1.0d);
    }

    @Test
    void generateOne() throws BadDatumException {
        final SymbolCalendar calendar = new SymbolCalendar("TNA");
        final double startingPrice = 10.0d;
        final int howMany = 20 * 6;  // ~ 6 months

        generator.fillCalendar(calendar, LocalDateTime.now(ZoneId.of("US/Eastern")), startingPrice, howMany);
        assertEquals(howMany, calendar.size());

        calendar.printHistory();
        calendar.printChart();
    }

    @Test
    void generateMany() throws BadDatumException {
        int i = 100;
        while (i-- > 0)
            generateOne();

        assertTrue(true);
    }
}