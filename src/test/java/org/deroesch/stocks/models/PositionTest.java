/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import org.deroesch.stocks.models.exceptions.SymbolMismatchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class PositionTest {
    private static final String ID1 = "PositionTest ID 1";
    private static final String ID2 = "PositionTest ID 2";

    Position position1;
    Position position2;

    final Account account = new Account("Account ID", 100000.0);
    final LocalDateTime openDate = LocalDateTime.of(2021, 1, 1, 0, 0, 0);

    @BeforeEach
    void beforeEach() {
        position1 = new Position(ID1, account, Datum.UNKNOWN_SYMBOL, openDate);
        position2 = new Position(ID2, account, Datum.UNKNOWN_SYMBOL, openDate);
    }

    @Test
    void isEmpty() {
        assertFalse(position1.isEmpty());
    }

    @Test
    void symbolMismatchInAddTransaction() {
        final Transaction tx = new Transaction("ATT", openDate, 100, new Price(Datum.OPEN, 10.0));
        assertThrows(SymbolMismatchException.class, () -> position1.addTransaction(tx));
    }

    @Test
    void testToString() {
        assertEquals(
                "Position[id='PositionTest ID 1', account=Account[id='Account ID', state=IDLE, balance=100000.0], symbol='????', openDate=2021-01-01T00:00, closeDate=null, shares=0, amount=0.0]",
                position1.toString());
    }

    @Test
    void testEquals() {
        assertEquals(position1, position1);
        assertEquals(position2, position2);
        assertNotEquals(position1, position2);
    }

    @Test
    void testHashCode() {
        assertEquals(position1.hashCode(), position1.hashCode());
        assertNotEquals(position1.hashCode(), position2.hashCode());
    }

    @Test
    void getId() {
        assertEquals(ID1, position1.getId());
    }

    @Test
    void getAccount() {
        assertEquals(account, position1.getAccount());
    }

    @Test
    void getSymbol() {
        assertEquals(Datum.UNKNOWN_SYMBOL, position1.getSymbol());
    }

    @Test
    void getOpenDate() {
        assertEquals(openDate, position1.getOpenDate());
    }


    @Test
    void getCloseDate() {
        assertNull(position1.getCloseDate());
    }

    @Test
    void getShares() {
        assertEquals(0, position1.getShares());
    }

    @Test
    void getAmount() {
        assertEquals(0.0, position1.getAmount());
    }

    @Test
    void getTransactions() {
        assertTrue(position1.getTransactions().isEmpty());
    }
}