/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import org.deroesch.stocks.models.empties.EmptyPrice;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class PriceTest {

    final Price price = new Price(Datum.HIGH, 10.0);
    final Price empty = EmptyPrice.get();

    @Test
    void getValue() {
        assertEquals(10.0, price.getValue());
    }

    @Test
    void getName() {
        assertEquals(Datum.HIGH, price.getRole());
    }

    @Test
    void isEmpty() {
        assertTrue(empty.isEmpty());
        assertFalse(price.isEmpty());
    }


    @Test
    void compare() {
        assertTrue(Price.compare(price, empty) > 0);
        assertTrue(Price.compare(empty, price) < 0);
        assertEquals(0, Price.compare(price, price));
    }

    @Test
    void empty() {
        assertNotEquals(empty, price);
        assertEquals(0.0, empty.getValue());
    }

    @Test
    void isAbove() {
        assertTrue(price.isAbove(empty));
        assertFalse(price.isAbove(price));
    }

    @Test
    void isBelow() {
        assertTrue(empty.isBelow(price));
        assertFalse(empty.isBelow(empty));
    }

    @Test
    void direction() {
        assertEquals(Price.Compares.STEADY, price.direction(price));
        assertEquals(Price.Compares.LOWER, empty.direction(price));
        assertEquals(Price.Compares.HIGHER, price.direction(empty));
    }

    /*
     * For equals() defined as:
     *
     * if (this == o) return true;
     * if (o == null || getClass() != o.getClass()) return false;
     * final Price price = (Price) o;
     * return Double.compare(price.value, value) == 0;
     */
    @Test
    void equals() {
        final Price nullPrice = null; // Need this var to pacify Sonarqube code smell complaints

        assertEquals(price, price);
        assertNotEquals(price, empty);
        assertNotEquals(price, nullPrice);
        assertNotEquals(price, new Object());
    }

    @Test
    void fullyEquals() {
        final Price p1 = new Price(Datum.HIGH, 10.0);
        final Price p2 = new Price(Datum.HIGH, 10.0);
        final Price p3 = new Price(Datum.LOW, 10.0);
        final Price p4 = new Price(Datum.LOW, 11.0);

        assertTrue(p1.fullyEquals(p2));
        assertFalse(p2.fullyEquals(p3));
        assertFalse(p3.fullyEquals(p4));
    }

    @Test
    void plusPercent() {
        assertEquals(11, price.plusPercent(0.10));
        assertEquals(5.0, price.plusPercent(-.50));
        assertEquals(10, price.plusPercent(0.0));
    }

    @Test
    void testHashCode() {
        assertEquals(empty.hashCode(), EmptyPrice.get().hashCode());
        assertNotEquals(empty.hashCode(), price.hashCode());
    }

    @Test
    void testToString() {
        assertEquals(" 0.00", empty.toString());
        assertEquals("10.00", price.toString());
    }
}