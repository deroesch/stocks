/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models.empties;

import org.deroesch.stocks.models.Datum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class EmptyDatumTest {

    Datum d;

    @BeforeEach
    void beforeEach() {
        d = EmptyDatum.get();
    }

    @Test
    void get() {
        assertEquals(EmptyDatum.get(), d);
    }

    @Test
    void isEmpty() {
        assertTrue(d.isEmpty());
    }

    /**
     * Demonstrate that setters don't have any effect.
     */
    @Test
    void setters() {
        final String symbol = "ATT";

        d.setSymbol(symbol);
        d.setPeriod(LocalDateTime.now());
        d.setOpen(1);
        d.setClose(2);
        d.setAdjustedClose(3);
        d.setHigh(4);
        d.setLow(5);
        d.setVolume(6);

        assertEquals(EmptyDatum.get(), d);
    }
}