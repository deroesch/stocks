/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models.empties;

import org.deroesch.stocks.models.Datum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class EmptySymbolCalendarTest {

    final Datum emptyDatum = EmptyDatum.get();
    EmptySymbolCalendar calendar;

    @BeforeEach
    void setUp() {
        calendar = EmptySymbolCalendar.get();
    }

    @Test
    void isEmpty() {
        assertTrue(calendar.isEmpty());
    }

    @Test
    void getFirstPeriod() {
        assertEquals(LocalDateTime.MAX, calendar.getFirstPeriod());
    }

    @Test
    void getFinalPeriod() {
        assertEquals(LocalDateTime.MIN, calendar.getFinalPeriod());
    }

    @Test
    void getByIndex() {
        assertTrue(calendar.get(0).isEmpty());
    }

    @Test
    void getByDate() {
        assertTrue(calendar.get(LocalDateTime.now()).isEmpty());
    }

    @Test
    void put() {

        assertThrows(UnsupportedOperationException.class, () -> calendar.put(emptyDatum));
    }

    @Test
    void getWeeklyCalendar() {
        assertEquals(EmptySymbolCalendar.get(), calendar.getWeeklyCalendar());
    }

    @Test
    void getMonthlyCalendar() {
        assertEquals(EmptySymbolCalendar.get(), calendar.getMonthlyCalendar());
    }

    @Test
    void size() {
        assertEquals(0, calendar.size());
    }

    @Test
    void getSymbol() {
        assertEquals(Datum.UNKNOWN_SYMBOL, calendar.getSymbol());
    }

    @Test
    void getLastIndex() {
        assertEquals(0, calendar.getLastIndex());
    }
}