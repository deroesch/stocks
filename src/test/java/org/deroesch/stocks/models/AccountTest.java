/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import org.deroesch.stocks.models.exceptions.ClosedPositionException;
import org.deroesch.stocks.models.exceptions.DateOrderException;
import org.deroesch.stocks.models.exceptions.NonEmptyAssignmentException;
import org.deroesch.stocks.models.exceptions.SymbolMismatchException;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

class AccountTest {
    private static final String ID1 = "AccountTest ID 1";
    private static final String ID2 = "AccountTest ID 2";
    private static final float BALANCE = 100000.0f;

    Account account1;
    Account account2;

    @BeforeEach
    void beforeEach() {
        account1 = new Account(ID1, BALANCE);
        account2 = new Account(ID2, BALANCE / 2.0);
    }

    @Test
    void addTransaction()
            throws
            NonEmptyAssignmentException,
            SymbolMismatchException,
            DateOrderException,
            ClosedPositionException {
        final Account account = new Account("Account ID", 100000.0);

        assertEquals(100000, account.getBalance());
        assertTrue(account.getOpenPositions().isEmpty());
        assertTrue(account.getClosedPositions().isEmpty());

        // Verify position 'ATT' doesn't exist yet
        assertNull(account.getOpenPositions().get("ATT"));

        final LocalDateTime openDate = LocalDateTime.now().truncatedTo(ChronoUnit.DAYS);
        final Price price = new Price(Datum.CLOSE, 15);

        // Add transaction att01 to open position 'ATT'
        final Transaction att01 = new Transaction("ATT", openDate, 1000, price);
        account.addTransaction(att01);

        // Verify position 'ATT' doesn't was created
        final Position att = account.getOpenPositions().get("ATT");
        assertNotNull(att);
        assertEquals(Position.State.OPEN, att.getState());
        assertTrue(att.isOpen());
        assertFalse(att.isClosed());

        assertEquals(1, account.getOpenPositions().size());
        assertEquals(0, account.getClosedPositions().size());

        // Verify transaction att01 is part of the 'ATT' position, with account balance
        // adjusted by the cost of the purchase.
        assertEquals(att01, att.getTransactions().get(0));
        assertEquals(85000, account.getBalance());

        // Add transaction att02 to add to position 'ATT'
        final Transaction att02 = new Transaction("ATT", openDate, 1000, price);
        account.addTransaction(att02);

        // Verify transaction att02 is part of the 'ATT' position, with account balance
        // adjusted by the cost of the second purchase.
        assertEquals(att02, att.getTransactions().get(1));
        assertEquals(70000, account.getBalance());

        // Re-adding a transaction that was already added somewhere is disallowed, should throw.
        assertThrows(NonEmptyAssignmentException.class,
                () -> account.addTransaction(att01));

        // Add transaction dow01 to open position 'DOW'
        final Transaction dow01 = new Transaction("DOW", openDate, 1000, price);
        account.addTransaction(dow01);
        assertEquals(55000, account.getBalance());

        // Add transaction dow02 to add to position 'DOW'
        final Transaction dow02 = new Transaction("DOW", openDate, 1000, price);
        account.addTransaction(dow02);
        assertEquals(40000, account.getBalance());

        assertEquals(2, account.getOpenPositions().size());
        assertEquals(40000, account.getBalance());

        // Close position 'ATT'
        assertThrows(DateOrderException.class, () ->
                att.close(openDate.minusDays(1), new Price(Datum.CLOSE, 0.0)));

        att.close(openDate.plusDays(1), new Price(Datum.CLOSE, 16));

        assertThrows(ClosedPositionException.class, () ->
                att.close(openDate.plusDays(1), new Price(Datum.CLOSE, 16)));

        // 40000 current balance + 32000 sale
        assertEquals(72000, account.getBalance());
        assertEquals(2000, att.getProfit());

        // Close position 'DOW'
        final Position dow = account.getOpenPositions().get("DOW");
        dow.close(openDate.plusDays(1), new Price(Datum.CLOSE, 20));

        // 72000 current balance + 40000 sale
        assertEquals(112000, account.getBalance());
        assertEquals(10000, dow.getProfit());
    }

    @Test
    void testToString() {
        assertEquals("Account[id='AccountTest ID 1', state=IDLE, balance=100000.0]", account1.toString());
    }

    @Test
    void testEquals() {
        assertEquals(account1, account1);
        assertEquals(account2, account2);
        assertNotEquals(account1, account2);
    }

    @Test
    void testHashCode() {
        assertEquals(account1.hashCode(), account1.hashCode());
        assertNotEquals(account1.hashCode(), account2.hashCode());
    }

    @Test
    void getId() {
        assertTrue(account1.getId().length() > 0);
    }

    @Test
    void getState() {
        assertEquals(Account.State.IDLE, account1.getState());
    }

    @Test
    void getBalance() {
        assertEquals(BALANCE, account1.getBalance());
    }

    @Test
    void getOpenPositions() {
        assertEquals(0, account1.getOpenPositions().size());
    }

    @Test
    void getClosedPositions() {
        assertEquals(0, account1.getClosedPositions().size());
    }
}