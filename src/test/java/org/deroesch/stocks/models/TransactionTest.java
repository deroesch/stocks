/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import org.deroesch.stocks.models.empties.EmptyPosition;
import org.deroesch.stocks.models.empties.EmptyTransaction;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

class TransactionTest {

    private static final String ID1 = "TransactionTest ID 1";
    private static final String ID2 = "TransactionTest ID 2";

    Transaction transaction1;
    Transaction transaction2;
    final Price price = new Price(Datum.CLOSE, 25.00);
    final LocalDateTime timestamp = LocalDateTime.of(2021, 1, 1, 0, 0, 0);
    final Account account = new Account("Account 1", 1000000.0);
    final Position position = new Position("Positions 1", account, Datum.UNKNOWN_SYMBOL, timestamp);

    @BeforeEach
    void setUp() {
        transaction1 = new Transaction(ID1, Datum.UNKNOWN_SYMBOL, timestamp, 100, price);
        transaction2 = new Transaction(ID2, Datum.UNKNOWN_SYMBOL, timestamp, 10, price);
    }

    @Test
    void isEmpty() {
        assertFalse(transaction1.isEmpty());
        assertTrue(EmptyTransaction.get().isEmpty());
    }

    @Test
    void getAmount() {
        assertEquals(2500.0, transaction1.getAmount());
        assertEquals(250.0, transaction2.getAmount());
    }

    @Test
    void testToString() {
        assertEquals(
                "Transaction[id='TransactionTest ID 1', symbol='????', timestamp=2021-01-01T00:00, shares=100, price=25.00, amount=2500.0]",
                transaction1.toString());
    }

    @Test
    void testEquals() {
        assertEquals(transaction1, transaction1);
        assertNotEquals(transaction1, transaction2);
        assertNotEquals(transaction1, new Object());
    }

    @Test
    void testHashCode() {
        assertEquals(transaction1.hashCode(), transaction1.hashCode());
        assertNotEquals(transaction1.hashCode(), transaction2.hashCode());
    }

    @Test
    void getId() {
        assertEquals(ID1, transaction1.getId());
        assertEquals(ID2, transaction2.getId());
    }

    @Test
    void getPosition() {
        assertEquals(EmptyPosition.get(), transaction1.getPosition());
    }

    @Test
    void getSymbol() {
        assertEquals(Datum.UNKNOWN_SYMBOL, transaction1.getSymbol());
        assertEquals(Datum.UNKNOWN_SYMBOL, transaction2.getSymbol());
    }

    @Test
    void getTimestamp() {
        assertEquals(timestamp, transaction1.getTimestamp());
        assertEquals(timestamp, transaction2.getTimestamp());
    }

    @Test
    void getShares() {
        assertEquals(100, transaction1.getShares());
        assertEquals(10, transaction2.getShares());
    }

    @Test
    void getPrice() {
        assertEquals(25.0, transaction1.getPrice().getValue());
        assertEquals(25.0, transaction2.getPrice().getValue());
    }

    @Test
    void setPosition() {
        assertEquals(EmptyPosition.get(), transaction1.getPosition());
        transaction1.setPosition(position);
        assertEquals(position, transaction1.getPosition());
    }
}