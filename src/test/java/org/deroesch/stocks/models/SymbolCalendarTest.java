/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import lombok.extern.slf4j.Slf4j;
import org.deroesch.stocks.models.exceptions.BadDatumException;
import org.deroesch.stocks.tools.DatumGenerator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;

import static org.junit.jupiter.api.Assertions.*;

@Slf4j
class SymbolCalendarTest {

    final static String GOOD_SYMBOL = "GOOD";
    final static String BAD_SYMBOL = "BAD";

    Datum datum;
    SymbolCalendar calendar;

    @BeforeEach
    void setUp() {
        datum = new Datum();
        calendar = new SymbolCalendar(GOOD_SYMBOL);

        assertFalse(calendar.isEmpty());
    }

    @Test
    void getAndPut() {
        // Set up a calendar and datum with matching symbols.
        datum.setSymbol(GOOD_SYMBOL);
        datum.setPeriod(LocalDateTime.now());

        try {
            // Insert the good datum and verify its presence.
            calendar.put(datum);
            assertEquals(datum, calendar.get(datum.getPeriod()));

            // Try ot insert a datum with the wrong symbol.  This will throw.
            datum.setSymbol(BAD_SYMBOL);
            datum.setPeriod(datum.getPeriod().plusDays(1));
            calendar.put(datum);
        } catch (final BadDatumException e) {
            assertEquals(BAD_SYMBOL, e.getDatum().getSymbol());
        }
    }

    @Test
    void emptyFetches() {
        Datum d;
        d = calendar.get(10);
        assertTrue(d.isEmpty());

        d = calendar.get(LocalDateTime.now());
        assertTrue(d.isEmpty());
    }

    @Test
    void dateBrackets() throws BadDatumException {
        final DatumGenerator d = new DatumGenerator();
        final int count = 3;
        final Datum other = new Datum();
        other.setSymbol(calendar.getSymbol());

        // Set up the initial span of days
        final LocalDateTime today = LocalDateTime.of(2021, 8, 2, 0, 0);
        d.fillCalendar(calendar, today, 1.0, count);
        assertEquals(today, calendar.getFirstPeriod());
        assertEquals(today.plusDays(count - 1), calendar.getFinalPeriod());

        // Add a before after the initial span
        final LocalDateTime newFirstDay = today.minusDays(1);
        other.setPeriod(newFirstDay);
        calendar.put(other);
        assertEquals(newFirstDay, calendar.getFirstPeriod());

        // Add a date after the initial span
        final LocalDateTime newFinalDay = today.plusDays(count);
        other.setPeriod(newFinalDay);
        calendar.put(other);
        assertEquals(newFinalDay, calendar.getFinalPeriod());
    }
}