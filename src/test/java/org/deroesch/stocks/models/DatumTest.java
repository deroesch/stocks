/*
 * Copyright (c) 2021. Douglas E. Roesch
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */

package org.deroesch.stocks.models;

import org.deroesch.stocks.models.empties.EmptyDatum;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

import static org.junit.jupiter.api.Assertions.*;

class DatumTest {

    static final Datum EMPTY_DATUM = EmptyDatum.get();
    Datum d;

    @BeforeEach
    void beforeEach() {
        d = new Datum();
    }

    @Test
    void testFromCsv() {
        final String symbol = "TNA";
        final String line = "2008-12-05,6.107500,7.292500,5.652500,7.210000,6.111570,37032800";

        d = Datum.fromCsvLine(symbol, line);

        assertEquals(symbol, d.getSymbol());
        assertEquals(LocalDateTime.of(2008, 12, 5, 4, 0), d.getPeriod());
        assertEquals(Integer.parseInt("37032800"), d.getVolume());

        double delta = 0.01;    // Adjust for rounding.  Verify less than one penny (one per hundred).

        assertEquals(Double.parseDouble("6.107500"), d.getPrice(Datum.OPEN).getValue(), delta);
        assertEquals(Double.parseDouble("7.292500"), d.getPrice(Datum.HIGH).getValue(), delta);
        assertEquals(Double.parseDouble("5.652500"), d.getPrice(Datum.LOW).getValue(), delta);
        assertEquals(Double.parseDouble("7.210000"), d.getPrice(Datum.CLOSE).getValue(), delta);
        assertEquals(Double.parseDouble("6.111570"), d.getPrice(Datum.ADJUSTED_CLOSE).getValue(), delta);
    }

    @Test
    void testToString() {
        LocalDateTime datetime = LocalDateTime.of(2000, 1, 2, 3, 4, 5);
        double high = 1;
        double low = 2;
        double open = 3;
        double close = 4;
        double adjustedClose = 5;
        long volume = 6;

        Datum d = new Datum("ABC", datetime, high, low, open, close, adjustedClose, volume);

        assertFalse(d.isEmpty());
        assertEquals("ABC", d.getSymbol());
        assertEquals(Datum.UNKNOWN_SYMBOL, new Datum().getSymbol());
        assertEquals(high, d.getValue(Datum.HIGH));
        assertEquals(low, d.getValue(Datum.LOW));
        assertEquals(open, d.getValue(Datum.OPEN));
        assertEquals(close, d.getValue(Datum.CLOSE));
        assertEquals(adjustedClose, d.getValue(Datum.ADJUSTED_CLOSE));
        assertEquals(volume, d.getVolume());

        assertEquals("Datum[Symbol: ABC, Day: SUNDAY, Period: 2000-01-02_03:04, Delta: 1.00, Open: 3.00, Close: 4.00, High: 1.00, Low: 2.00, AdjClose: 5.00, Volume:6]",
                d.toString());
    }

    @Test
    void getPriceChangePercent() {
        d.setOpen(10);
        d.setClose(11);
        assertEquals(0.1, d.getPriceChangePercent());

        d.setClose(5);
        assertEquals(-0.5, d.getPriceChangePercent());
    }

    @Test
    void isStartOfWeek() {
        d.setPeriod(LocalDateTime.of(2021, 8, 2, 0, 0));
        assertTrue(d.isStartOfWeek());

        d.setPeriod(d.getPeriod().plusDays(1));
        assertFalse(d.isStartOfWeek());
    }

    @Test
    void isEndOfWeek() {
        d.setPeriod(LocalDateTime.of(2021, 8, 6, 0, 0));
        assertTrue(d.isEndOfWeek());

        d.setPeriod(d.getPeriod().plusDays(1));
        assertFalse(d.isEndOfWeek());
    }

    @Test
    void isStartOfMonth() {
        d.setPeriod(LocalDateTime.of(2021, 8, 1, 0, 0));
        assertTrue(d.isStartOfMonth());

        d.setPeriod(d.getPeriod().plusDays(1));
        assertFalse(d.isStartOfMonth());
    }

    @Test
    void isEndOfMonth() {
        d.setPeriod(LocalDateTime.of(2021, 2, 28, 0, 0));
        assertTrue(d.isEndOfMonth());

        d.setPeriod(LocalDateTime.of(2021, 6, 30, 0, 0));
        assertTrue(d.isEndOfMonth());

        d.setPeriod(LocalDateTime.of(2021, 12, 31, 0, 0));
        assertTrue(d.isEndOfMonth());

        d.setPeriod(d.getPeriod().plusDays(1));
        assertFalse(d.isEndOfMonth());
    }

    @Test
    void getPrintedPeriod() {
        d.setPeriod(LocalDateTime.now());
        final String s = d.getPrintedPeriod();
        assertEquals(s, d.getPrintedPeriod(ChronoUnit.MINUTES));
    }

    @Test
    void hash() {
        assertEquals(EMPTY_DATUM.hashCode(), EMPTY_DATUM.hashCode());
        assertNotEquals(EMPTY_DATUM.hashCode(), new Datum().hashCode());
    }

    @SuppressWarnings("ConstantConditions")
    @Test
    void equals() {

        final Datum nullDatum = null; // Need this var to pacify Sonarqube code smell complaints

        assertEquals(d, d);
        assertNotEquals(d, nullDatum);
        assertNotEquals(d, new Object());

        Datum o = new Datum();
        o.setPeriod(d.getPeriod());
        assertEquals(d, o);

        o.setSymbol("Hello");
        assertNotEquals(d, o);

        o.setVolume(1);
        assertNotEquals(d, o);

        o.setAdjustedClose(1);
        assertNotEquals(d, o);

        o.setClose(1);
        assertNotEquals(d, o);

        o.setClose(1);
        assertNotEquals(d, o);

        o.setOpen(1);
        assertNotEquals(d, o);

        o.setLow(1);
        assertNotEquals(d, o);

        o.setHigh(1);
        assertNotEquals(d, o);

        o.setPeriod(d.getPeriod().plusDays(1));
        assertNotEquals(d, o);
    }

    @Test
    void weekdayWeekend() {
        final LocalDateTime saturday = LocalDateTime.of(2021, 7, 31, 0, 0);
        final LocalDateTime sunday = saturday.plusDays(1);
        final LocalDateTime monday = saturday.plusDays(2);

        d.setPeriod(saturday);
        assertTrue(d.isWeekend());
        assertFalse(d.isWeekday());

        d.setPeriod(sunday);
        assertTrue(d.isWeekend());
        assertFalse(d.isWeekday());

        for (int i = 0; i < 5; i++) {
            d.setPeriod(monday.plusDays(i));
            assertTrue(d.isWeekday());
            assertFalse(d.isWeekend());
        }
    }
}